---
title: About
layout: default
---

# {{ page.title }}

I started this site to track my progress both using and attempting to create software/workflows to increase my freetime and decrease my constant checking of internet resources. It may spiral into something else, or it may end up as ust a list of bookmarks and bash one-liners for my own reference.
