---
title: Main Page
layout: default
---

# {{ page.title }}

this is a page

# Blog Posts

<ul>
	{% for post in site.posts %}
		<li>
			<a href="{{ post.url }}">{{ post.title }}</a>
		</li>	
	{% endfor %}
</ul>

[test](test/test.md)
