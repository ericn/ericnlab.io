---
title: Useful Apps
layout: default
---

# {{ page.title }}

---

## Linux

### Documents/Documentation
- [Pandoc](https://pandoc.org) - Convert any document filetype to any document filetype
- [groff](https://www.gnu.org/software/groff/) - Produce simple pdf files from simple markup languages 
- [Calibre](https://calibre-ebook.com) - EPUB/PDF file manager for E-Readers/Computers
- [VIM](http://www.vim.org) - THE cli Text Editior 
- [Youtube-DL](http://youtube-dl.org) - Download Video/Audio from Multiple [supported](https://ytdl-org.github.io/youtube-dl/supportedsites.html) Sites

### Media
- [FFMpeg](https://www.ffmpeg.org) - Convert most video/audio files (Good for x265 compression)
- [MVC-HC](http://mpv-hc.org) - Open Source Media player 
- [Audacity](http://www.audacityteam.org) - Basic Audio Editor (Simple, GUI)

### Terminal/Linux
- [Tmux](https://github.com/tmux/tmux) - Terminal Multiplexed (Leave SSH sessions running, spilt terminal Screens)
- [Termux](https://termux.com) - Terminal emulator/ Linux environment for Android
- [Ranger](https://github.com/ranger/ranger) - Great terminal file manager
- [lf](https://github.com/gokcehan/lf) - much like ranger, but written in Go and with Windows executable
- [Rclone](https://rclone.org/) - Rsync for cloud storage 
- [Rsync](https://rsync.samba.org/) - sync files between folders/devices/all
- [Resilio Sync](https://www.resilio.com/) - Bittorrent based file syncing
- [Aria2c](https://aria2.github.io/) - Download HTTP/FTP/SFTP/Bittorrent/Metalink files simply
- [Qute Browser](http://qutebrowser.org/) - Browser, with VIM Bindings EVERYWHERE
- [Python](https://www.python.org/) - Scripting language for everything
- [Newboat](https://newsboat.org/) - Command Line RSS/Podcast Manager
- [OpenVPN](https://openvpn.net/) - DIY VPN Solution
- [Brave](https://brave.com) - Chromium based browser with built-in adblocker
- [Transmission](https://transmissionbt.com) - Simple Bittorrent Client
- [udiskie](https://github.com/coldfix/udiskie) - Simple USB daemon for Linux
- [TaskWarrior](https://taskwarrior.org/) - Manage all your tasks from the command lind

### Learning
- [Anki](https://apps.ankiweb.net) - Intelligent flashcards, learn anything through spaced repatition

### Internet
- []()


---

## Windows
- [Fl Studio](https://www.image-line.com/flstudio/) - Digital Audio Workstation
- [Chocolatey](https://chocolatey.org/) - Package Manager on Windows
- [MegaSync](https://mega.nz/sync) - Sync Files across Multiple Devices

## Android

### General
- [Termux]() - 
- [SunVox]() - 
- [JuiceSSH]() - 
- [UserLAnd]() - 
- [NIX]() - 
- [Moon+ Reader]() - 
- [Foreground]() - 
- [DroidVim]() - 
- [Caustic]() - 
- [AnkiDroid]() - 
- [F-Droid]() - 
- [Smart Audiobook Player]() - 
- [MobMuPlat]() - 
- [jQuarks]() - 
- [Orgzly]() -
- [Tiny Tiny RSS]() -  


### Documents
- [Markor]() - Markdown Editor (Share links to quicknotes file, todo.txt support)

### Internet
- [MEGASync]() - Sync files to and from phone with Megasync
- [Newpipe]() - Youtube viewer with adblock, download and background mode
- [SimpleTask]() - 
- [TaskWarrior]() - 
- 
