---
title: Books
layout: default
---

# {{ page.title }}

## A list of Books in Currently Reading/Want to Read

<table>
<tr>
<th>Title</th>
<th>Author</th>
<th>Year</th>
<th>Genre</th>
<th>Type</th>
</tr>
{% for book in site.data.books %}
<tr>
<td>{{ book.Name }}</td>
<td>{{ book.Author }}</td>
<td>{{ book.Year }}</td>
<td>{{ book.Genre }}</td>
<td>{{ book.Type }}</td>
</tr>
{% endfor %}
</table>
